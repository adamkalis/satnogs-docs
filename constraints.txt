# This is a generated file; DO NOT EDIT!
#
# Please edit 'requirements.txt' or/and 'requirements-dev.txt'
# and run './contrib/refresh-requirements.sh' to regenerate
# this file

Babel==2.14.0
Jinja2==3.1.3
MarkupSafe==2.1.5
Pygments==2.17.2
Sphinx==7.2.6
alabaster==0.7.16
cachetools==5.3.2
certifi==2024.2.2
chardet==5.2.0
charset-normalizer==3.3.2
colorama==0.4.6
distlib==0.3.8
doc8==0.11.2
docutils==0.18.1
filelock==3.13.1
idna==3.6
imagesize==1.4.1
packaging==23.2
pbr==6.0.0
platformdirs==4.2.0
pluggy==1.4.0
polib==1.2.0
pyproject-api==1.6.1
regex==2023.12.25
requests==2.31.0
restructuredtext-lint==1.4.0
snowballstemmer==2.2.0
sphinx-lint==0.9.1
sphinx-rtd-theme==1.3.0
sphinxcontrib-applehelp==1.0.8
sphinxcontrib-devhelp==1.0.6
sphinxcontrib-htmlhelp==2.0.5
sphinxcontrib-jquery==4.1
sphinxcontrib-jsmath==1.0.1
sphinxcontrib-qthelp==1.0.7
sphinxcontrib-serializinghtml==1.1.10
stevedore==5.1.0
tox==4.13.0
urllib3==2.2.0
vale==3.0.0.0
versioneer==0.29
virtualenv==20.25.0

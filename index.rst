#####################
SatNOGS documentation
#####################

.. toctree::
   :maxdepth: 2
   :caption: 🎨 Design
   :hidden:

   Station Architecture <station_architecture>

.. toctree::
   :maxdepth: 2
   :caption: 🚀 Project
   :hidden:

   project/principles
   project/roles
   project/milestones

.. toctree::
   :maxdepth: 2
   :caption: 🛠️ Contribution
   :hidden:

   contribution/development
   contribution/documentation
   contribution/reporting-issues

.. toctree::
   :maxdepth: 2
   :caption: 🐝 Collaboration
   :hidden:

   collaboration/meetings

.. toctree::
   :maxdepth: 2
   :caption: 📨 Communication
   :hidden:

   Channels <communication/communication-channels>

.. toctree::
   :maxdepth: 2
   :caption: 📁 Subprojects
   :hidden:

   SatNOGS Config <https://docs.satnogs.org/projects/satnogs-config/en/stable>
   SatNOGS Client <https://docs.satnogs.org/projects/satnogs-client/en/stable>
   SatNOGS DB <https://docs.satnogs.org/projects/satnogs-db/en/stable>
   SatNOGS Network <https://docs.satnogs.org/projects/satnogs-network/en/stable>

.. toctree::
   :maxdepth: 2
   :caption: 📚 References
   :hidden:

   license

*****
Intro
*****

.. image:: /_images/satnogs.png
   :scale: 60%
   :align: right

The SatNOGS project, is a participatory open source project aiming to create
and utilize a distributed network of ground stations around the globe.

Project development spans across a wide spectrum of fields including hardware
development, software development, and data processing.

This is the development documentation for the `SatNOGS project
<https://satnogs.org>`_.

If you are looking for documentation on how to use SatNOGS or build a ground
station, please visit `the wiki <https://wiki.satnogs.org>`_.

* :ref:`search`
